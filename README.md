Jekyll Blog Importer
=======================


![Build Status](http://jsm85.cloudapp.net:8085/app/rest/builds/buildType:JekyllBlogImporter_1BuildAndTest/statusIcon)

## Background ##
The Jekyll Blog Importer tool was built to make migrating blog content from sites such as Wordpress to a Jekyll site easy.

**NOTE:** *This tool assumes you have already created a Jekyll Blog Site.*

## Current Supported Blog Sites ##

- Wordpress
- Blogger (Coming soon)

## Usage ##

The utility requires 2 arguments

- ```-s``` The source location of the xml source file (i.e. from Wordpress)
- ```-p``` The destination of the posts (This is an optional parameter - If left blank will write the posts to the directory where the application is being run from)

Used like so:

    jekyll-importer.exe -s[[SOURCE XML FILE]] -p [[POSTS LOCATION]]


Here is an example:
    
    jekyll-importer.exe -s C:\Blog\blog.xml -p C:\Blog\_posts



﻿using System;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;
using JekyllBlogImporter.Console.Services;
using Moq;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests.Services
{
    [TestFixture]
    public class FileDownloadServiceTests : TestBase
    {
        private Mock<IWebHelper> mockWebHelper;
        private FileDownloadService fileDownloadService;

        [Test]
        public void DownloadAttachments_should_set_new_url_and_download_file()
        {
            mockWebHelper = new Mock<IWebHelper>();
            fileDownloadService = new FileDownloadService(mockWebHelper.Object);

            var @params = CreateJekyllImporterParams("Test.xml", @".\TestPostLocation\");

            var jekyllPostAttachment = new JekyllPostAttachment(0, "Title.jpg", 1, DateTime.Now, "http:\\someUrl.jpg");

            mockWebHelper.Setup(x => x.DownloadFiles(jekyllPostAttachment)).Verifiable();
            fileDownloadService.DownloadAtatchments(@params, jekyllPostAttachment);

            Assert.IsNotNullOrEmpty(jekyllPostAttachment.DownloadLocation);
            mockWebHelper.Verify(x => x.DownloadFiles(jekyllPostAttachment));
        }
    }
}

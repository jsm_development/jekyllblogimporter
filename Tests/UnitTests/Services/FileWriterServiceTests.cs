using System;
using System.Collections.Generic;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;
using JekyllBlogImporter.Console.Services;
using Moq;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests.Services
{
    [TestFixture]
    public class FileWriterServiceTests : TestBase
    {
        private Mock<IJekyllMarkdownFileWriter> mockJekyllFileWriter;
        private FileWriterService fileDownloadService;
        private Mock<IJekyllMarkdownFileUtility> mockJekyllFileUtility;

        [Test]
        public void WriteMarkdownFile_should_create_a_friendly_name_and_then_write_the_markdown_file()
        {
            mockJekyllFileWriter = new Mock<IJekyllMarkdownFileWriter>();
            mockJekyllFileUtility = new Mock<IJekyllMarkdownFileUtility>();
            fileDownloadService = new FileWriterService(mockJekyllFileUtility.Object, mockJekyllFileWriter.Object);

            var @params = CreateJekyllImporterParams("Test.xml", @".\TestPostLocation\");

            var jekyllPost = new JekyllPost(0, "Title", "FileName", DateTime.Now, new List<string>(), "Some Content");

            var returnFileName = "TestFileName";

            mockJekyllFileUtility.Setup(x => x.CreateFriendlyJekyllFileName(jekyllPost)).Returns(returnFileName).Verifiable();
            mockJekyllFileWriter.Setup(x => x.Write(@params, jekyllPost, returnFileName)).Verifiable();


            fileDownloadService.WriteMarkdownFile(@params, jekyllPost);

            
            mockJekyllFileUtility.Verify(x => x.CreateFriendlyJekyllFileName(jekyllPost));
            mockJekyllFileWriter.Verify(x => x.Write(@params, jekyllPost, returnFileName));
        }
    }
}
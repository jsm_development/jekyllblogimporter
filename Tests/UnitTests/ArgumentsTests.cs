﻿using System.IO;
using JekyllBlogImporter.Console;
using JekyllBlogImporter.Console.Exceptions;
using JekyllBlogImporter.Console.Helpers;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests
{
    [TestFixture]
    public class ArgumentsTests
    {
        [Test]
        [ExpectedException(typeof(NoArgumentsException))]
        public void Ensure_NoArgumentsException_is_thrown_when_all_3_arguments_are_missing()
        {
            var arguments = new Arguments(new IoHelper(),  new string[]{});
            arguments.EnsureArgumentsExist();
        }

        [Test]
        [ExpectedException(typeof(FileNotFoundException))]
        public void Ensure_FileNotFoundException_is_thrown_when_all_source_file_does_not_exist()
        {
            var arguments = new Arguments(new IoHelper(), new string[] { "-s", "abc", "-p", "def", "-a", "ghi"});
            arguments.EnsureSourceFileExists();
        }
    }
}
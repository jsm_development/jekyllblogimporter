using System.Linq;
using System.Xml.Linq;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests.Extractors
{
    [TestFixture]
    public class WordPressExtractorTests : TestBase
    {
        [Test]
        public void When_I_Load_The_Xml_File_I_Should_Get_A_Valid_XDocument_Object()
        {
            var output = BlogExtractor.LoadExtractFile(TestFile);

            var result = output.Root.Elements().Elements("item").Count();

            Assert.That(result == 4);
        }

        [Test]
        public void When_I_extract_published_posts_from_document_that_has_2_published_posts_I_should_get_a_list_of_elements_with_a_total_of_2_published_posts()
        {
            var output = BlogExtractor.LoadExtractFile(TestFile);

            var result = BlogExtractor.ExtractPublishedPostsFromDocument(output);
            
            Assert.That(result.Count() == 2);
        }

        [Test]
        public void When_I_extract_post_attachments_from_document_that_has_1_attachement_item_I_should_get_a_list_of_elements_with_a_total_of_1_attachment()
        {
            var output = BlogExtractor.LoadExtractFile(TestFile);

            var result = BlogExtractor.ExtractPostAttachmentsFromDocument(output);

            Assert.That(result.Count() == 1);
        }
    }
}
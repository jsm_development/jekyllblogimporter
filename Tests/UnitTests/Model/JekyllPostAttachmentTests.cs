using System;
using JekyllBlogImporter.Console.Model;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests.Model
{
    [TestFixture]
    public class JekyllPostAttachmentTests : TestBase
    {
        [Test]
        public void SetNewJekyllPostAttachmentLocation_should_update_the_newurl_the_new_location_on_the_destination_machine()
        {
            var jekyllPostId = 1;
            var attachmentPostId = 0;
            var attachmentUrl = "http://SomeUrl.com/SomeImage.png";

            var jekyllPostAttachment = new JekyllPostAttachment(attachmentPostId, "TestImage.png", jekyllPostId,DateTime.Now, attachmentUrl);

            var @params = CreateJekyllImporterParams(@"C:\Test\Test.xml", @"C:\Test\Posts\");

            jekyllPostAttachment.SetNewJekyllPostAttachmentLocation(@params);

            Assert.That(jekyllPostAttachment.DownloadLocation.Equals(@"C:\Test\Posts\..\assets\content\0_TestImage.png"));
            Assert.That(jekyllPostAttachment.RelativeLocation.Equals(@"/assets/content/0_TestImage.png"));
        }
    }
}
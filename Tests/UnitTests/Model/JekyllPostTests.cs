﻿using System;
using System.Collections.Generic;
using JekyllBlogImporter.Console.Model;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests.Model
{
    [TestFixture]
    public class JekyllPostTests : TestBase
    {
        [Test]
        public void ReplaceOldGistLinkWithJekyllGistLink_should_replace_old_gist_link_with_new_one()
        {
            var content = @"This is a Test with a Gist Link

                            https://gist.github.com/12345

                            Please replace the link!";

            var jekyllPost = CreateJekyllPost(0, content);

            jekyllPost.ReplaceOldGistLinkWithJekyllGistLink();

            Assert.That(jekyllPost.Content.Contains("{% gist 12345 %}"));
        }

        [Test]
        public void ReplaceOldAttachmentLinksWithNewLocation_should_replace_old_attachment_link_with_new_one()
        {
            var jekyllPostId = 1;
            var attachmentPostId = 0; 
            var attachmentUrl = "http://SomeUrl.com/SomeImage.png";
            string content = string.Format(@"This is a Test with an attachment that has a link
                                
                                {0}

                                Please replace the link!", attachmentUrl);


            var jekyllPostAttachment = new JekyllPostAttachment(attachmentPostId, "TestImage.png", jekyllPostId,
                                                                DateTime.Now, attachmentUrl);

            var @params = CreateJekyllImporterParams(TestFile, @".\Test\");

            jekyllPostAttachment.SetNewJekyllPostAttachmentLocation(@params);

            var attachments = new List<JekyllPostAttachment>() {jekyllPostAttachment};
            
            var jekyllPost = CreateJekyllPost(jekyllPostId, content);
            jekyllPost.ReplaceOldAttachmentLinksWithNewLocation(attachments);

            Assert.That(jekyllPost.Content.Contains(@"/assets/content/0_TestImage.png"));
        }

        private static JekyllPost CreateJekyllPost(int postId, string content)
        {
            var jekyllPost = new JekyllPost(postId,
                                            "TestTitle",
                                            "TestFileName",
                                            DateTime.Now,
                                            new List<string>() { "Test" },
                                            content);
            return jekyllPost;
        }
    }
}

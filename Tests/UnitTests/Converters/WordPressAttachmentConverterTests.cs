using System.Linq;
using JekyllBlogImporter.Console.Converters;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests.Converters
{
    [TestFixture]
    public class WordPressAttachmentConverterTests : TestBase
    {
        [Test]
        public void Convert_should_get_fully_construct_JekyllPostAttachment_object()
        {
            var document = BlogExtractor.LoadExtractFile(TestFile);
            var elements = BlogExtractor.ExtractPublishedPostsFromDocument(document);

            var result = new WordPressPostAttachmentConverter().Convert(elements.First());

            Assert.IsNotNull(result);
        }
    }
}
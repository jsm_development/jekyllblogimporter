﻿using System;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.UnitTests.Helpers
{
    [TestFixture]
    public class JekyllMarkdownFileUtilityTests
    {
        private JekyllMarkdownFileUtility jekyllFileNameUtitlity;

        [SetUp]
        public void SetUp()
        {
            jekyllFileNameUtitlity = new JekyllMarkdownFileUtility();
        }

        [Test]
        public void CreateFriendlyJekyllFileName_should_build_a_filename_that_is_YYYY_MM_DD_Title()
        {

            var jekyllPost = CreateJekyllPost("Test Title", DateTime.Parse("01 Jan 2013"));

            var result = jekyllFileNameUtitlity.CreateFriendlyJekyllFileName(jekyllPost);

            Assert.That(result.Equals("2013-01-01-test-title"));
        }

        [Test]
        public void CreateFriendlyJekyllFileName_should_build_a_filename_that_is_YYYY_MM_DD_Title_when_day_and_month_greater_than_September()
        {

            var jekyllPost = CreateJekyllPost("Test Title", DateTime.Parse("31 Oct 2013"));

            var result = jekyllFileNameUtitlity.CreateFriendlyJekyllFileName(jekyllPost);

            Assert.That(result.Equals("2013-10-31-test-title"));
        }

        [Test]
        public void CreateFriendlyJekyllFileName_should_remove_any_characters_that_cause_jekyll_to_fail()
        {
            var jekyllPost = CreateJekyllPost("Test Title… With Some Funky Characters.–", DateTime.Parse("01 Jan 2013"));

            var result = jekyllFileNameUtitlity.CreateFriendlyJekyllFileName(jekyllPost);

            Assert.That(result.Equals("2013-01-01-test-title-with-some-funky-characters"));
        }

        private static JekyllPost CreateJekyllPost(string testTitle, DateTime postDate)
        {
            var jekyllPost = new JekyllPost(0, testTitle, "TestFileName", postDate, null, "");
            return jekyllPost;
        }
    }
}

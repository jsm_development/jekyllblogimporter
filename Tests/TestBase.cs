using System.IO;
using JekyllBlogImporter.Console;
using JekyllBlogImporter.Console.Extractors;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;
using JekyllBlogImporter.Console.Services;
using Moq;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests
{

    public class TestBase
    {
        public IIoHelper IoHelper;
        public Mock<IWebHelper> MockWebHelper;
        public IBlogExtractor BlogExtractor;
        public IJekyllMarkdownFileWriter FileWriter;
        public IJekyllMarkdownFileUtility FileUtility;
        public BlogMigrationService BlogMigrationService;
        public IFileDownloadService FileDownloadService;
        public IFileWriterService FileWriterService;

        [TestFixtureSetUp]
        public void SetUp()
        {
            IoHelper = new IoHelper();
            MockWebHelper = new Mock<IWebHelper>();
            BlogExtractor = new WordpressBlogExtractor();
            FileUtility = new JekyllMarkdownFileUtility();
            FileWriter = new JekyllMarkdownFileWriter();
            FileDownloadService = new FileDownloadService(MockWebHelper.Object);
            FileWriterService = new FileWriterService(FileUtility, FileWriter);
            BlogMigrationService = new BlogMigrationService(IoHelper, BlogExtractor, FileDownloadService, FileWriterService);
        }

        protected const string TestFile = @".\WordpressExportExample.xml";

        public void CleanDirectories()
        {
            if (Directory.Exists(@".\_posts"))
                Directory.Delete(@".\_posts", true);
            if (Directory.Exists(@".\assets")) 
                Directory.Delete(@".\assets", true);
        }

        protected static JekyllImporterParams CreateJekyllImporterParams(string inputFile, string postDirectory)
        {
            return new JekyllImporterParams(new Arguments(new IoHelper(), new string[]
                                                                    {
                                                                        "-s",
                                                                        inputFile,
                                                                        "-p",
                                                                        postDirectory
                                                                    }));
        }
    }
}
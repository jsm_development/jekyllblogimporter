﻿using System.IO;
using System.Linq;
using JekyllBlogImporter.Console;
using JekyllBlogImporter.Console.Model;
using NUnit.Framework;

namespace JekyllBlogImporter.Tests.IntegrationTests
{
    [TestFixture]
    public class BlogMigrationServiceTests : TestBase
    {
        [Test]
        public void When_I_Import_A_Wordpress_Extract_Xml_File_With_2_Post_Entries_I_Should_See_2_Posts_In_A_Posts_Folder()
        {
            CleanDirectories();
            string postDirectory = @".\_posts";

            var @params = CreateJekyllImporterParams(TestFile, postDirectory);

            BlogMigrationService.Migrate(@params);

            var directory = new DirectoryInfo(@".\_posts");
            var fileNames = directory.GetFiles("*.markdown").Select(x => x.Name).ToList();

            Assert.That(fileNames.Count == 2);
        }
    }
}

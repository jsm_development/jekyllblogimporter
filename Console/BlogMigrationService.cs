using System;
using System.Linq;
using JekyllBlogImporter.Console.Converters;
using JekyllBlogImporter.Console.Extractors;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;
using JekyllBlogImporter.Console.Services;

namespace JekyllBlogImporter.Console
{
    public class BlogMigrationService
    {
        private readonly IIoHelper ioHelper;
        private readonly IBlogExtractor blogExtractor;
        private readonly IFileDownloadService fileDownloadService;
        private readonly IFileWriterService fileWriterService;

        public BlogMigrationService(
            IIoHelper ioHelper,
            IBlogExtractor blogExtractor,
            IFileDownloadService fileDownloadService, 
            IFileWriterService fileWriterService)
        {
            if (ioHelper == null) throw new ArgumentNullException("ioHelper");
            if (blogExtractor == null) throw new ArgumentNullException("blogExtractor");
            if (fileDownloadService == null) throw new ArgumentNullException("fileDownloadService");
            if (fileWriterService == null) throw new ArgumentNullException("fileWriterService");
            this.ioHelper = ioHelper;
            this.blogExtractor = blogExtractor;
            this.fileDownloadService = fileDownloadService;
            this.fileWriterService = fileWriterService;
        }

        public void Migrate(JekyllImporterParams @params)
        {
            ConsoleHelper.DisplayMessage(string.Format("Beginning Migration of {0} to jekyll", @params.InputFile));
            
            ConsoleHelper.DisplayMessage("Extracting data from Xml file and building jekyll objects.");
            var document = blogExtractor.LoadExtractFile(@params.InputFile);
            var publishedPosts = blogExtractor.ExtractPublishedPostsFromDocument(document);
            var jekyllPosts = publishedPosts.Select(new WordPressPostConverter().Convert).ToList();
            var attachments = blogExtractor.ExtractPostAttachmentsFromDocument(document);
            var jekyllPostAttachments = attachments.Select(new WordPressPostAttachmentConverter().Convert).ToList();

            ConsoleHelper.DisplayMessage("Creating Assets Directory");
            ioHelper.CreateDirectoryIfItDoesNotAlreadyExist(@params.AssetsDownloadLocation);
            
            foreach (var jekyllPostAttachment in jekyllPostAttachments)
            {
                ConsoleHelper.DisplayProgressMessage(string.Format("Downloading attachment {0} of {1}",
                                                                   ConsoleHelper.CurrentItemCounter(jekyllPostAttachments.IndexOf(jekyllPostAttachment)),
                                                                   jekyllPostAttachments.Count));
                fileDownloadService.DownloadAtatchments(@params, jekyllPostAttachment);
            }

            ConsoleHelper.DisplayMessage("");
            ConsoleHelper.DisplayMessage("Creating Posts Directory");
            ioHelper.CreateDirectoryIfItDoesNotAlreadyExist(@params.PostsLocation);

            foreach (var jekyllPost in jekyllPosts)
            {
                ConsoleHelper.DisplayProgressMessage(string.Format("Creating file {0} of {1}",
                                                   ConsoleHelper.CurrentItemCounter(jekyllPosts.IndexOf(jekyllPost)),
                                                   jekyllPosts.Count));
                jekyllPost.ReplaceOldAttachmentLinksWithNewLocation(jekyllPostAttachments);
                jekyllPost.ReplaceOldGistLinkWithJekyllGistLink();
                fileWriterService.WriteMarkdownFile(@params, jekyllPost);
            }
            ConsoleHelper.DisplayMessage("");
            ConsoleHelper.DisplayMessage("Migration completed succesfully!");
        }
    }
}
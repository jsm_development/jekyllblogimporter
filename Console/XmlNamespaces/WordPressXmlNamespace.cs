using System.Xml.Linq;

namespace JekyllBlogImporter.Console.XmlNamespaces
{
    public class WordPressXmlNamespace
    {
        public static XNamespace Export
        {
            get
            {
                return XNamespace.Get("http://wordpress.org/export/1.2/");
            }
        }

        public static XNamespace Content
        {
            get
            {
                return XNamespace.Get("http://purl.org/rss/1.0/modules/content/");
            }
        }
        
    }
}
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using JekyllBlogImporter.Console.XmlNamespaces;

namespace JekyllBlogImporter.Console.Extractors
{
    public class WordpressBlogExtractor : IBlogExtractor
    {

        public IEnumerable<XElement> ExtractPostAttachmentsFromDocument(XDocument document)
        {
            IEnumerable<XElement> attachments =
                document.Root.Elements()
                    .Elements("item")
                    .Where(x => x.Element(WordPressXmlNamespace.Export + "post_type").Value == "attachment"
                                && x.Element(WordPressXmlNamespace.Export + "status").Value == "inherit")
                    .ToList();
            return attachments;
        }

        public IEnumerable<XElement> ExtractPublishedPostsFromDocument(XDocument document)
        {
            IEnumerable<XElement> items =
                document.Root.Elements()
                    .Elements("item")
                    .Where(x => x.Element(WordPressXmlNamespace.Export + "post_type").Value == "post"
                                && x.Element(WordPressXmlNamespace.Export + "status").Value == "publish")
                    .ToList();
            return items;
        }

        public XDocument LoadExtractFile(string fileLocation)
        {
            XDocument document;
            using (var reader = new XmlTextReader(fileLocation))
                document = XDocument.Load(reader);
            return document;
        }
    }
}
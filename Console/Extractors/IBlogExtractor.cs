using System.Collections.Generic;
using System.Xml.Linq;

namespace JekyllBlogImporter.Console.Extractors
{
    public interface IBlogExtractor
    {
        IEnumerable<XElement> ExtractPostAttachmentsFromDocument(XDocument document);
        IEnumerable<XElement> ExtractPublishedPostsFromDocument(XDocument document);
        XDocument LoadExtractFile(string fileLocation);
    }
}
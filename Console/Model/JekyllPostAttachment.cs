﻿using System;

namespace JekyllBlogImporter.Console.Model
{
    public class JekyllPostAttachment
    {
        public JekyllPostAttachment(int postId, string title, int parentPostId, DateTime postDate, string attachmentUrl)
        {
            AttachmentUrl = attachmentUrl;
            PostDate = postDate;
            ParentPostId = parentPostId;
            Title = title;
            PostId = postId;
        }

        public int PostId { get; private set; }
        public string Title { get; private set; }
        public string DownloadLocation { get; private set; }
        public string RelativeLocation { get; private set; }
        public int ParentPostId { get; private set; }
        public DateTime PostDate { get; private set; }
        public string AttachmentUrl { get; private set; }

        public void SetNewJekyllPostAttachmentLocation(JekyllImporterParams @params)
        {
            var newFileName = string.Format("{0}_{1}", PostId, Title);
            DownloadLocation = string.Format("{0}{1}", @params.AssetsDownloadLocation, newFileName);
            RelativeLocation = string.Format("{0}{1}", @params.AssetsRelativeLocation, newFileName);
        }
    }
}
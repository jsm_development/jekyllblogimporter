﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Html2Markdown;

namespace JekyllBlogImporter.Console.Model
{
    public class JekyllPost
    {
        public JekyllPost(int postId, string title, string fileName, DateTime postDate, List<string> categories, string htmlContent)
        {
            Title = title;
            PostId = postId;
            PostDate = postDate;
            FileName = fileName;
            Categories = categories;

            ConvertHtmlContentToMarkdown(htmlContent);
        }

        public int PostId { get; private set; }
        public string Title { get; private set; }
        public string FileName { get; private set; }
        public DateTime PostDate { get; private set; }
        public string Content { get; private set; }
        public List<string> Categories { get; private set; }

        public void ConvertHtmlContentToMarkdown(string htmlContent)
        {
            var convertedContent = new Converter().Convert(htmlContent);
            Content = convertedContent;
        }

        public void ReplaceOldAttachmentLinksWithNewLocation(IEnumerable<JekyllPostAttachment> jekyllPostAttachments)
        {
            foreach (var jekyllPostAttachment in jekyllPostAttachments.Where(x => x.ParentPostId == PostId))
                Content = Content.Replace(jekyllPostAttachment.AttachmentUrl, jekyllPostAttachment.RelativeLocation);
        }   

        public void ReplaceOldGistLinkWithJekyllGistLink()
        {
            var gistLinkRegex = new Regex(@"https://gist.github.com/\d+", RegexOptions.IgnoreCase);
            var matchCollection = gistLinkRegex.Matches(Content);
            
            foreach (Match match in matchCollection)
            {
                var oldGistLink = match.Value;
                var gistId = new Regex(@"\d+").Match(oldGistLink).Value;

                if(!string.IsNullOrEmpty(gistId))
                {
                    var newGistLink = string.Format(@"{{% gist {0} %}}", gistId);
                    Content = Content.Replace(oldGistLink, newGistLink);
                }
            }
        }
    }
}
namespace JekyllBlogImporter.Console.Model
{
    public class JekyllImporterParams
    {
        public JekyllImporterParams(Arguments @params)
        {
            var source = @params.Values["Source"];
            var postsLocation = @params.Values["PostDirectory"];


            if (string.IsNullOrEmpty(postsLocation))
                postsLocation = @"\_posts\";

            if (!postsLocation.EndsWith(@"\"))
                postsLocation = postsLocation + "\\";

            InputFile = source;
            PostsLocation = postsLocation;
            AssetsDownloadLocation = string.Format(@"{0}..\assets\content\", postsLocation);
            AssetsRelativeLocation = "/assets/content/";
        }

        public string InputFile { get; private set; }
        public string PostsLocation { get; private set; }
        public string AssetsDownloadLocation { get; private set; }
        public string AssetsRelativeLocation { get; private set; }
    }
}
using System.IO;

namespace JekyllBlogImporter.Console.Helpers
{
    public interface IIoHelper
    {
        void CreateDirectoryIfItDoesNotAlreadyExist(string directory);
        bool CheckIfFileExists(string file);
    }

    public class IoHelper : IIoHelper
    {
        public void CreateDirectoryIfItDoesNotAlreadyExist(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        public bool CheckIfFileExists(string file)
        {
            return File.Exists(file);
        }
    }
}
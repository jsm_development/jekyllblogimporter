using System.Text;
using JekyllBlogImporter.Console.Model;

namespace JekyllBlogImporter.Console.Helpers
{
    public interface IJekyllMarkdownFileUtility
    {
        string CreateFriendlyJekyllFileName(JekyllPost jekyllPost);
    }

    public class JekyllMarkdownFileUtility : IJekyllMarkdownFileUtility
    {
        private string[] UnwantedFileNameCharacters = new string[] { "�", ".", "�" };

        public string CreateFriendlyJekyllFileName(JekyllPost jekyllPost)
        {
            var fileNameBuilder = new StringBuilder(jekyllPost.Title.ToLower());
            
            RemoveUnwantedCharactersFromTitle(fileNameBuilder);
            ReplaceWhiteSpacesWithHypons(fileNameBuilder);

            return string.Format("{0}-{1}-{2}-{3}",
                                 jekyllPost.PostDate.Year,
                                 jekyllPost.PostDate.Month.ConvertToDoubleDigit(),
                                 jekyllPost.PostDate.Day.ConvertToDoubleDigit(),
                                 fileNameBuilder);
        }

        private void ReplaceWhiteSpacesWithHypons(StringBuilder fileNameBuilder)
        {
            fileNameBuilder.Replace(" ", "-");
        }

        private void RemoveUnwantedCharactersFromTitle(StringBuilder fileNameBuilder)
        {
            foreach (var character in UnwantedFileNameCharacters)
                fileNameBuilder = fileNameBuilder.Replace(character, "");
        }
    }

    public static class FileNameExtentsions
    {
        public static string ConvertToDoubleDigit(this int input)
        {
            return input <= 9 ? string.Format("0{0}", input) : input.ToString();
        }
    }
}
using System.Net;
using JekyllBlogImporter.Console.Model;

namespace JekyllBlogImporter.Console.Helpers
{
    public interface IWebHelper
    {
        void DownloadFiles(JekyllPostAttachment jekyllPostAttachment);
    }
    
    public class WebHelper : IWebHelper
    {
        public void DownloadFiles(JekyllPostAttachment jekyllPostAttachment)
        {
            using (var client = new WebClient())
                client.DownloadFile(jekyllPostAttachment.AttachmentUrl, jekyllPostAttachment.DownloadLocation);
        }
    }
}
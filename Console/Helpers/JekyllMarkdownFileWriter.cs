using System.IO;
using System.Text;
using JekyllBlogImporter.Console.Model;

namespace JekyllBlogImporter.Console.Helpers
{
    public interface IJekyllMarkdownFileWriter
    {
        void Write(JekyllImporterParams @params, JekyllPost jekyllPost, string fileName);
    }

    public class JekyllMarkdownFileWriter : IJekyllMarkdownFileWriter
    {
        public void Write(JekyllImporterParams @params, JekyllPost jekyllPost, string fileName)
        {
            using (var writer = new StreamWriter(string.Format(@"{0}{1}.markdown", @params.PostsLocation, fileName), false, Encoding.GetEncoding(850)))
            {
                writer.WriteLine("---");
                writer.WriteLine("layout:       post");
                writer.WriteLine(string.Format("title:        {0}", jekyllPost.Title));
                writer.WriteLine(string.Format("date:         {0}", jekyllPost.PostDate));
                writer.WriteLine(string.Format("categories:   {0}", string.Join(" ", jekyllPost.Categories)));
                writer.WriteLine("---");
                writer.WriteLine();

                writer.Write(jekyllPost.Content);
            }
        }
    }
}
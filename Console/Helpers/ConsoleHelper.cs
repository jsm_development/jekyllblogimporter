﻿using System;

namespace JekyllBlogImporter.Console.Helpers
{
    public class ConsoleHelper
    {
        public static void DisplayHelp()
        {
            System.Console.ResetColor();
            System.Console.WriteLine("=== Jekyll Blog Importer ===");
            System.Console.WriteLine("");
            System.Console.WriteLine("Usage:");
            System.Console.WriteLine("");
            System.Console.WriteLine("-s    -   Location of the source file.");
            System.Console.WriteLine("-p    -   (Optional) Location of where you want the posts to be exported to.");
            System.Console.WriteLine("");
            System.Console.WriteLine("");
            System.Console.WriteLine("Example:");
            System.Console.WriteLine("");
            System.Console.WriteLine(@"jekyll-importer.exe -s C:\Blog\WordPress.xml -p C:\Blog\_posts");
            System.Console.WriteLine("");
        }

        public static void DisplayError(string message)
        {
            System.Console.ForegroundColor = ConsoleColor.Red;
            System.Console.WriteLine("::::: ERROR :::::");
            System.Console.WriteLine("");
            System.Console.WriteLine(String.Format("MESSAGE: {0}", message));
            System.Console.WriteLine("");
            System.Console.ResetColor();
        }

        public static void DisplayMessage(string message)
        {
            System.Console.WriteLine(message);
        }

        public static void DisplayProgressMessage(string message)
        {
            System.Console.Write("\r" + message);
        }

        public static void ResetColour()
        {
            System.Console.ResetColor();
        }

        public static int CurrentItemCounter(int indexedInt)
        {
            return indexedInt + 1;
        }
    }
}
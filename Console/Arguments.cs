﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JekyllBlogImporter.Console.Exceptions;
using JekyllBlogImporter.Console.Helpers;

namespace JekyllBlogImporter.Console
{
    public class Arguments
    {
        private readonly IIoHelper ioHelper;
        public Dictionary<string, string> Values;


        public Arguments(IIoHelper ioHelper, string[] args)
        {
            this.ioHelper = ioHelper;
            if (ioHelper == null) throw new ArgumentNullException("ioHelper");
            

            Values = new Dictionary<string, string>();

            for (var i = 0; i < args.Count(); i = i + 2)
            {
                if (args[i].StartsWith("-") && !args[i + 1].StartsWith("-"))
                {
                    switch (args[i])
                    {
                        case "-s":
                            Values.Add("Source", args[i + 1]);
                            break;
                        case "-p":
                            Values.Add("PostDirectory", args[i + 1]);
                            break;
                    }
                }

            }
        }
        
        public void EnsureArgumentsExist()
        {
            if (Values.Count != 2)
                ThrowNoArgumentsException();
        }

        public void EnsureSourceFileExists()
        {
            var fileExists = ioHelper.CheckIfFileExists(Values["Source"]);
            if (!fileExists)
                ThrowNoFileExistsException();
        }

        private void ThrowNoArgumentsException()
        {
            throw new NoArgumentsException(
                "You do not have all the required arguments in order to execute the application");
        }

        private void ThrowNoFileExistsException()
        {
            throw new FileNotFoundException(
                "The source file does not exist. Please ensure that a valid file is selected");
        }
    }
}
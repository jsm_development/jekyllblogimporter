using System;
using System.Xml.Linq;
using JekyllBlogImporter.Console.Model;
using JekyllBlogImporter.Console.XmlNamespaces;

namespace JekyllBlogImporter.Console.Converters
{
    public class WordPressPostAttachmentConverter : IConverter<JekyllPostAttachment>
    {
        public JekyllPostAttachment Convert(XElement attachment)
        {
            var postIdValue = 0;
            var titleValue = "";
            var parentPostIdValue = 0;
            var attachmentUrlValue = "";
            var postDateValue = DateTime.Now;

            var title = attachment.Element("title");
            if (title != null)
                titleValue = title.Value;

            var postId = attachment.Element(WordPressXmlNamespace.Export + "post_id");
            if (postId != null)
                postIdValue = int.Parse(postId.Value);

            var parentPostId = attachment.Element(WordPressXmlNamespace.Export + "post_parent");
            if (parentPostId != null)
                parentPostIdValue = int.Parse(parentPostId.Value);

            var postDate = attachment.Element("pubDate");
            if (postDate != null)
                postDateValue = System.Convert.ToDateTime(postDate.Value);

            var attachmentUrl = attachment.Element(WordPressXmlNamespace.Export + "attachment_url");
            if (attachmentUrl != null)
                attachmentUrlValue = attachmentUrl.Value;

            var newAttachment = new JekyllPostAttachment(postIdValue,
                                                         titleValue,
                                                         parentPostIdValue,
                                                         postDateValue,
                                                         attachmentUrlValue);
            return newAttachment;
        }
    }
}
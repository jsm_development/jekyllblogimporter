using System;
using System.Linq;
using System.Xml.Linq;
using JekyllBlogImporter.Console.Model;
using JekyllBlogImporter.Console.XmlNamespaces;

namespace JekyllBlogImporter.Console.Converters
{
    public class WordPressPostConverter : IConverter<JekyllPost>
    {
        public JekyllPost Convert(XElement xElement)
        {
            var postIdValue = 0;
            var titleValue = "";
            var fileNameValue = "";
            var htmlContentValue = "";
            var postDateValue = DateTime.Now;

            var title = xElement.Element("title");
            if (title != null)
                titleValue = title.Value;

            var postId = xElement.Element(WordPressXmlNamespace.Export + "post_id");
            if (postId != null)
                postIdValue = int.Parse(postId.Value);

            var postDate = xElement.Element("pubDate");
            if (postDate != null)
                postDateValue = System.Convert.ToDateTime(postDate.Value);

            var fileName = xElement.Element(WordPressXmlNamespace.Export + "post_name");
            if (fileName != null)
                fileNameValue = fileName.Value;

            var content = xElement.Element(WordPressXmlNamespace.Content + "encoded");
            if (content != null)
                htmlContentValue = content.Value;

            var categoryValues = xElement.Elements("category").Select(x => x.Value).ToList();

            var newPost = new JekyllPost(postIdValue,
                                         titleValue,
                                         fileNameValue,
                                         postDateValue,
                                         categoryValues,
                                         htmlContentValue);
            return newPost;
        }
    }
}
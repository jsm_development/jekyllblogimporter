using System.Xml.Linq;

namespace JekyllBlogImporter.Console.Converters
{
    public interface IConverter<T>
    {
        T Convert(XElement input);
    }
}
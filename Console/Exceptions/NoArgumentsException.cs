namespace JekyllBlogImporter.Console.Exceptions
{
    public class NoArgumentsException : System.Exception
    {
        public NoArgumentsException(string message) : base(message)
        {

        }
    }
}
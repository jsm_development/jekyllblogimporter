using System;
using System.Collections.Generic;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;

namespace JekyllBlogImporter.Console.Services
{
    public interface IFileWriterService
    {
        void WriteMarkdownFile(JekyllImporterParams @params, JekyllPost jekyllPost);
    }

    public class FileWriterService : IFileWriterService
    {
        private readonly IJekyllMarkdownFileUtility jekyllMarkdownFileUtility;
        private readonly IJekyllMarkdownFileWriter jekyllMarkdownFileWriter;

        public FileWriterService(IJekyllMarkdownFileUtility jekyllMarkdownFileUtility,
            IJekyllMarkdownFileWriter jekyllMarkdownFileWriter)
        {
            if (jekyllMarkdownFileUtility == null) throw new ArgumentNullException("jekyllMarkdownFileUtility");
            if (jekyllMarkdownFileWriter == null) throw new ArgumentNullException("jekyllMarkdownFileWriter");
            this.jekyllMarkdownFileUtility = jekyllMarkdownFileUtility;
            this.jekyllMarkdownFileWriter = jekyllMarkdownFileWriter;
        }

        public void WriteMarkdownFile(JekyllImporterParams @params, JekyllPost jekyllPost)
        {
            var fileName = jekyllMarkdownFileUtility.CreateFriendlyJekyllFileName(jekyllPost);
            jekyllMarkdownFileWriter.Write(@params, jekyllPost, fileName);
        }
    }
}
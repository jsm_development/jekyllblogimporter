using System;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;

namespace JekyllBlogImporter.Console.Services
{
    public interface IFileDownloadService
    {
        void DownloadAtatchments(JekyllImporterParams @params, JekyllPostAttachment jekyllPostAttachment);
    }

    public class FileDownloadService : IFileDownloadService
    {
        private readonly IWebHelper webHelper;

        public FileDownloadService(IWebHelper webHelper)
        {
            if (webHelper == null) throw new ArgumentNullException("webHelper");
            this.webHelper = webHelper;
        }

        public void DownloadAtatchments(JekyllImporterParams @params, JekyllPostAttachment jekyllPostAttachment)
        {
            jekyllPostAttachment.SetNewJekyllPostAttachmentLocation(@params);
            webHelper.DownloadFiles(jekyllPostAttachment);
        }
    }
}
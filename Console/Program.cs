﻿using System;
using JekyllBlogImporter.Console.Exceptions;
using JekyllBlogImporter.Console.Extractors;
using JekyllBlogImporter.Console.Helpers;
using JekyllBlogImporter.Console.Model;
using JekyllBlogImporter.Console.Services;

namespace JekyllBlogImporter.Console
{
    public class Program
    {
        private static IoHelper ioHelper;
        private static WebHelper webHelper;
        private static WordpressBlogExtractor blogExtractor;
        private static FileDownloadService fileDownloadService;
        private static FileWriterService fileWriterService;
        private static JekyllMarkdownFileUtility jekyllMarkdownFileUtility;
        private static JekyllMarkdownFileWriter jekyllMarkdownFileWriter;

        public static void Main(string[] args)
        {
            try
            {
                Initialize();

                var arguments = new Arguments(ioHelper, args);
                arguments.EnsureArgumentsExist();
                arguments.EnsureSourceFileExists();

                var @params = new JekyllImporterParams(arguments);

                new BlogMigrationService(ioHelper, blogExtractor, fileDownloadService, fileWriterService)
                    .Migrate(@params);
            }
            catch(Exception exception)
            {
                ConsoleHelper.DisplayError(exception.Message);
                ConsoleHelper.DisplayHelp();
            }
        }

        private static void Initialize()
        {
            ioHelper = new IoHelper();
            webHelper = new WebHelper();
            blogExtractor = new WordpressBlogExtractor();
            jekyllMarkdownFileWriter = new JekyllMarkdownFileWriter();
            jekyllMarkdownFileUtility = new JekyllMarkdownFileUtility();
            fileDownloadService = new FileDownloadService(webHelper);
            fileWriterService = new FileWriterService(jekyllMarkdownFileUtility, jekyllMarkdownFileWriter);
        }
    }
}